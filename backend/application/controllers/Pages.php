<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
// 1. CREO IL CONTROLLER PER LE PAGINE 
// - utile ad esempio per la creazione di pagine statiche
// - come linea guida, nel nominare il file .php del controller è consigliabile usare un nome plurale
// (quindi "Pages", con la maiuscola iniziale), mentre usare il singolare per il relativo Modello 
// 1.1 È UTILE CREARE ANCHE I TEMPLATE PER HEADER E FOOTER 
// CHE SARANNO COMUNI A TUTTA L'APPLICAZIONE (dentro "views/templates")
class Pages extends CI_Controller {	// creo la classe che estende la classe CI_Controller

	public function view($page = 'home') {	//gli passo la variabile $page assegnandoli un valore default
		// inizio verificando l'esistenza di una View di questa pagina
		if(!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
			// se la pagina non esisto mostro un 404
			show_404();	// funzione di CI
		}

		// la pagina sarà http://localhost/ci/pages/view/about - in questo url:
		// - "pages" è il controller
		// - "view" è il metodo
		// per togliere dall'url il percorso "pages/view" bisogna agire sulle Route in config 

		// creo una variabile Title all'interno dell'array $data
		// l'array "$data" rappresenta i dati (o le variabili) che intendiamo passare nella View
		// - creo la chiave "title" e le assegno il valore della variabile $page
		$data['title'] = ucfirst($page); // funzione CI per iniziale maiuscola

		// ora carico le view
		$this->load->view('templates/header');	// gli passo l'header
		$this->load->view('pages/' . $page, $data);	// gli passo la pagina isnieme a qualsiasi dato presente in $data
		$this->load->view('templates/footer');	// gli passo il footer
	}
}
