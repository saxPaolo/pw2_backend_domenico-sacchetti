<?php
class Auth extends CI_Controller {

    public function login() {
        $data['title'] = 'Login Page';
        $data['section'] = 'login';
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('password','Password','required');

        if($this->form_validation->run()===FALSE) {
            $this->load->view('templates/header',$data);
            $this->load->view('login');
            $this->load->view('templates/footer');
        } else {
            $this->load->model('user_model');
            if($this->user_model->check_user(
                $this->input->post('username'),
                $this->input->post('password')
                )
            )
            {
                //Login a buon fine
                $this->session->set_userdata('auth_ok', true);
                $this->session->set_userdata('auth_username', $this->input->post('username'));
                
                //flag superadmin
                $this->session->set_userdata('auth_superadmin', $this->user_model->is_superadmin($this->input->post('username')));
                
                redirect('news/index');
            } else {
                redirect('auth/login');
            }
        }
    }

    public function logout() {
        $this->session->unset_userdata('auth_ok');
        $this->session->unset_userdata('auth_username');
        redirect('auth/login');
    }
}
