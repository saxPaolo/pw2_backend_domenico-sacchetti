<?php
class Users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        
        if(!$this->session->auth_ok) {
            redirect('auth/login');
        } else {
            if(!$this->session->auth_superadmin) {
                redirect('news/index');
            }
        }
    }

    public function index()
    {
        $data['users'] = $this->user_model->get_all_users();
        $data['title'] = 'User management';
        $data['section'] = 'users_index';
        
        $this->load->view('common/header', $data);
        $this->load->view('users/index', $data);
        $this->load->view('common/footer');
    }

    /* CREATE USER */
    public function create() {
        $data['title'] = 'Create a new user';
        $data['section'] = 'user_create';

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username','Username','required|min_length[4]');
        $this->form_validation->set_rules('password','Password','required|min_length[8]');

        if($this->form_validation->run() === FALSE) {
            //Validazione fallita o form non inviato
            $this->load->view('common/header',$data);
            $this->load->view('users/create');
            $this->load->view('common/footer');
        } else {
            //Validazione ok
            $array_ass_dati = array(
                'username' => $this->input->post('username'), //$_POST['username']
                'password' => sha1($this->input->post('password')), //$_POST['password']
                'superadmin' => $this->input->post('superadmin') ? 1 : 0 //$_POST['superadmin']
            );

            $this->user_model->create_user($array_ass_dati);

            $this->session->set_flashdata('feedback', 'The user has been created!');

            redirect('users/index');
        }
    }

    // CODIFICA IN JSON PER API REST 
    public function json($id) {
        $user = $this->user_model->get_user($id);
        header('Content-type: application/json');
        echo json_encode($user);
    }

    /* EDIT USER */
    public function edit($id) {

        $data['user'] = $this->user_model->get_user($id);

        $data['title'] = 'Edit user';
        $data['section'] = 'user_edit';

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username','Username','required|min_length[4]');
        $this->form_validation->set_rules('password','Password','required|min_length[8]');

        if($this->form_validation->run() === FALSE) {
            //Validazione fallita o form non inviato
            $this->load->view('common/header',$data);
            $this->load->view('users/edit');
            $this->load->view('common/footer');
        } else {
            //Validazione ok
            $array_ass_dati = array(
                'username' => $this->input->post('username'), //$_POST['username']
                'password' => sha1($this->input->post('password')), //$_POST['password']
                'superadmin' => $this->input->post('superadmin') ? 1 : 0 //$_POST['superadmin']
            );

            $this->user_model->update_user($id, $array_ass_dati);

            $this->session->set_flashdata('feedback', 'The user has been updated!');

            redirect('users/index');

        }
    }

    /* DELETE USER */
    public function delete($id) {
        $this->user_model->delete_user($id);
        $this->session->set_flashdata('feedback', 'The user has been deleted!');
        redirect('users/index');
    }
}