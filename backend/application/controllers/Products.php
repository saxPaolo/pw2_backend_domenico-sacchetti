<?php
class Products extends CI_Controller {
        public function __construct()
        {
                parent::__construct();
                $this->load->model('product_model');

                $this->load->helper('url');

                /* if(!$this->session->auth_ok) {
                        redirect('auth/login');        
                } */
	}
	
	/* METODO GET ALL */
        public function index()
        {
                $data['products'] = $this->product_model->get_all_products();
                $data['title'] = 'Lista prodotti';
                $data['section'] = 'products_index';

                $this->load->view('templates/header', $data);
                $this->load->view('products/index', $data);
                $this->load->view('templates/footer');
        }

	/* METODO GET SINGLE */
        public function view($id = NULL)
        {
            $data['product_item'] = $this->product_model->get_single_product($id);
	    $data['title'] = $data['product_item']['prodname']; 
	    $data['desc'] = $data['product_item']['proddesc'];  
	    $data['price'] = $data['product_item']['prodprice'];  
	    $data['img'] = $data['product_item']['prodimg'];  
	    $data['section'] = 'product_item';

            if (empty($data['product_item']))
            {
                show_404();
            }

            $this->load->view('templates/header', $data);
            $this->load->view('products/view', $data);
            $this->load->view('templates/footer');
        }
	
	/* METODO CREATE */
        public function create() {

                $data['title'] = 'Crea nuovo prodotto';
                $data['section'] = 'product_create';

                $this->load->helper('form');
                $this->load->library('form_validation');

                $this->form_validation->set_rules('prodname','Nome','required');
                $this->form_validation->set_rules('proddesc','Descrizione','required');
                $this->form_validation->set_rules('prodprice','Prezzo','required');
                $this->form_validation->set_rules('prodimg','Immagine','required');

                if($this->form_validation->run() === FALSE) {

                        //Validazione fallita o form non inviato
                        $this->load->view('templates/header',$data);
                        $this->load->view('products/create');
			$this->load->view('templates/footer');
                } else {
                        //Validazione ok
                        $array_ass_dati = array(
                                'prodname' => $this->input->post('prodname'),
                                'proddesc' => $this->input->post('proddesc'),
                                'prodprice' => $this->input->post('prodprice'),
                                'prodimg' => $this->input->post('prodimg')
                        );
                        $this->product_model->create_product($array_ass_dati);
                        /* $this->load->view('templates/header',$data);
                        $this->load->view('products/index');
			$this->load->view('templates/footer'); */
			$this->session->set_flashdata('feedback', 'Il prodotto è stato creato!');

			redirect('products/index');
                }
	}

	/* METODO EDIT  */
	public function edit($id) {

		$data['products'] = $this->product_model->get_single_product($id);

                $data['title'] = 'Modifica prodotto con id #' . $id;
                $data['section'] = 'product_edit';

                $this->load->helper('form');
                $this->load->library('form_validation');

                $this->form_validation->set_rules('prodname','Nome','required');
                $this->form_validation->set_rules('proddesc','Descrizione','required');
                $this->form_validation->set_rules('prodprice','Prezzo','required');
                $this->form_validation->set_rules('prodimg','Immagine','required');

                if($this->form_validation->run() === FALSE) {
			// print_r($data);
                        //Validazione fallita o form non inviato
                        $this->load->view('templates/header',$data);
                        $this->load->view('products/edit');
			$this->load->view('templates/footer');
			
                } else {
                        
                        //Validazione ok
                        $array_ass_dati = array(				
				'prodname' => $this->input->post('prodname'),
                                'proddesc' => $this->input->post('proddesc'),
                                'prodprice' => $this->input->post('prodprice'),
                                'prodimg' => $this->input->post('prodimg')
                        );
			$this->product_model->update_product($id, $array_ass_dati);

			$this->session->set_flashdata('feedback', 'Il prodotto è stato aggiornato!');

			redirect('products/index');
		}
	}

	/* METODO DELETE */
	public function delete($id) {
		$this->product_model->delete_product($id);
		$this->session->set_flashdata('feedback', 'Il prodotto è stato eliminato!');
		redirect('products/index');
	}

	// CODIFICA IN JSON PER API REST 
	public function json($id) {
		$product = $this->product_model->get_product($id);
		header('Content-type: application/json');
		echo json_encode($product);
	}
}
