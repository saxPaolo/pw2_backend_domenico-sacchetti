<?php
class Orders extends CI_Controller {
        public function __construct()
        {
                parent::__construct();
                $this->load->model('order_model');

		$this->load->helper('url');
		$this->load->helper('date');

        }
	
	/* METODO GET ALL */
        public function index()
        {
                $data['orders'] = $this->order_model->get_all_orders();
                $data['title'] = 'Ordini';
                $data['section'] = 'orders_index';

                $this->load->view('templates/header', $data);
                $this->load->view('orders/index', $data);
                $this->load->view('templates/footer');
	}
	
	/* METODO GET SINGLE */
        /* public function view($id = NULL)
        {
	    $data['order_item'] = $this->order_model->get_single_order($id);
            $data['title'] = $data['order_item']['id'];
	    
	    $data['data_ordine'] = $data['order_item']['ordData'];
            $data['indirizzo'] = $data['order_item']['ordIndirizzo'];
            $data['citta'] = $data['order_item']['ordCitta'];
	    $data['email'] = $data['order_item']['ordEmail'];
	    $data['tel'] = $data['order_item']['ordTel'];
	    

            $data['section'] = 'order';

            if (empty($data['order_item']))
            {
                show_404();
	    }

            $this->load->view('templates/header', $data);
            $this->load->view('orders/view', $data);
            $this->load->view('templates/footer');
	} */

	public function view($id) {
                $data['order_item'] = $this->order_model->get_single_order($id);
                /* echo "<pre>";
                print_r($data);
                echo "</pre>"; */
                $data['section'] = 'order';

                if (empty($data['order_item']))
                {
			show_404();
                }

                $righe = $this->order_model->get_products($id);
                /* echo "<pre>";
                print_r($righe);
                echo "</pre>"; */

		$data['id'] = $data['order_item']['id'];
		$data['data_ordine'] = $data['order_item']['orddata'];
		$data['indirizzo'] = $data['order_item']['ordindirizzo'];
		$data['numero'] = $data['order_item']['ordstrnum'];
		$data['citta'] = $data['order_item']['ordcitta'];
		$data['email'] = $data['order_item']['ordemail'];
		$data['tel'] = $data['order_item']['ordtel'];
		$data['status'] = $data['order_item']['statusnome'];
		$data['status_icon'] = $data['order_item']['statusicon'];
                $data['status_class'] = $data['order_item']['statusclass'];
		$data['rows'] = $righe;
		
		if (empty($data['order_item']))
		{
			show_404();
		}

                $this->load->view('templates/header', $data);
                $this->load->view('orders/view', $data);
		$this->load->view('templates/footer');
		
        }

	/* METODO DELETE */
	public function delete($id) {
		$this->order_model->delete_order($id);
		$this->session->set_flashdata('feedback', 'L\'ordine è stato eliminato!');
		redirect('orders/index');
	}
}
