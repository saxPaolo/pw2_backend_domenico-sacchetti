<?php
class Product_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
	}
	
	// GET ALL
	public function get_all_products()
	{
		$this->db->select('*');
		$this->db->from('prodotti');
		$this->db->order_by("id", "desc");
		$query = $this->db->get(); 
		return $query->result_array();
	}

	// GET SINGLE
	public function get_single_product($id)
	{
		$query = $this->db->get_where('prodotti', array('id' => $id));
		return $query->row_array();
	}
		
	// CREATE
	public function create_product($array_ass_dati) {
		$this->db->insert('prodotti', $array_ass_dati);
	}

	/* DELETE */
    public function delete_product($id) {
        $this->db->delete('prodotti', array('id' => $id));
    }
	
	// UPDATE
	public function update_product($id, $array_ass_dati) {
		$this->db->where('id', $id);
		$this->db->update('prodotti', $array_ass_dati);
	}
}
