<?php

class User_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function check_user($username, $password) 
    {
        // $query = $this->db->get_where('users', array('username' => $password, 'password' => $password,));
        // return $query->row_array();
        $query = $this->db->from('users')
        ->where('user_username', $username)
        ->where('user_password', sha1($password))
        ->get();
        
        return $query->num_rows()==1;
    }

    public function is_superadmin($username) {
        $query = $this->db->from('users')
        ->where('user_username', $username)
        ->where('user_isadmin', true)
        ->get();
        
        return $query->num_rows()==1;
    }

    public function get_all_users()
    {
        $query = $this->db->get('users');
        return $query->result_array();
    }

    /* FUNZIONE CREATE USER */
    public function create_user($array_ass_dati) {
        $this->db->insert('users', $array_ass_dati);
    }
    
    /* FUNZIONE DELETE USER */
    public function delete_user($id) {
        $this->db->delete('users', array('user_id' => $id));
    }

    /* FUNZIONE UPDATE USER */
    public function update_user($id, $array_ass_dati) {
        $this->db->where('user_id', $id);
        $this->db->update('users', $array_ass_dati);
    }
    
    /* FUNZIONE GET USER */
    public function get_user($id) {
        $query = $this->db->get_where('users', array('user_id' => $id));
        return $query->row_array();
    }
}
