<?php
class Order_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
	}
	
	/* GET ALL */
	public function get_all_orders()
	{
		$this->db->select('*');
		$this->db->from('ordini');
		$this->db->join('statusordine', 'ordini.ordstatus = statusordine.id');
		$this->db->order_by("orddata", "desc");
		$query = $this->db->get(); 
		return $query->result_array();
	}

	/* public function get_all_orders()
	{
		$this->db->select('*');
		$this->db->from('ordini');
		$query = $this->db->get();
		return $query->result_array();
	} */
	/* public function get_all_orders()
	{
		$query = $this->db->get('ordini');
		return $query->result_array();
	} */

	/* GET SINGLE */
	public function get_single_order($id)
	{
		$this->db->select('*');
		$this->db->from('ordini');
		$this->db->join('statusordine', 'ordini.ordstatus = statusordine.id');
		$this->db->where('ordini.id', $id);
		$query = $this->db->get();
		return $query->row_array();
	}
	
	// GET DEI PRODOTTI
	public function get_products($id)
	{
		$this->db->select('*');
		$this->db->from('ordprod');
		$this->db->join('prodotti', 'prodotti.id = ordprod.idprod');
		$this->db->where('ordprod.idord', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	/* DELETE */
    public function delete_order($id) {
        $this->db->delete('ordini', array('id' => $id));
    }
}
