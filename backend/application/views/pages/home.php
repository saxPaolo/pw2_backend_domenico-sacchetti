<div class="d-flex justify-content-between py-5">
    <h2>
        <?php echo $title; ?>
    </h2>
</div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="flex-container">
				<div class="flex-item">
					<a href="<?php echo site_url('products/'); ?>">Gestione prodotti</a>
				</div>
				<div class="flex-item">
					<a href="<?php echo site_url('orders/'); ?>">Gestione Ordini</a>
				</div>
			</div>
		</div>
	</div>
</div>
