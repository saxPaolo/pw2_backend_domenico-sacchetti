<div class="d-flex justify-content-between py-5">
	<h2>
		<?php echo $title; ?>
	</h2>
</div>

<div class="media">
  <img width="50%" class="align-self-start mr-3" src="<?php echo asset_url();?>img/products/<?php echo $img; ?>" alt="<?php echo $title; ?>">
  <div class="media-body">
    <h5 class="mt-0"><?php echo $price; ?></h5>
    <p><?php echo $desc; ?></p>
  </div>
</div>

