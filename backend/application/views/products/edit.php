<div class="d-flex justify-content-between py-5">
	<h2>
		<?php echo $title; ?>
	</h2>
</div>

<?php $validation_error = validation_errors(); ?>
<?php if($validation_error) : ?>
<div class="alert alert-warning" role="alert">
    <?php echo $validation_error; ?>
</div>
<?php endif; ?>

<?php echo form_open('products/edit/' . $products['id']); ?>
    <fieldset class="py-3">
        <div class="form-group">
            <label for="prodname">Nome</label>
            <input id="prodname" type="text" class="form-control" name="prodname" value="<?php echo set_value('prodname', $products['prodname']);?>"/>
		</div>
		<div class="form-group">
			<label for="proddesc">Descrizione</label>
			<textarea id="proddesc" class="form-control" name="proddesc"><?php echo set_value('proddesc'), $products['proddesc'];?></textarea>
		</div>
		<div class="form-group">
			<label for="prodprice">Prezzo</label>
			<input id="prodprice" type="number" min="1" step="any"  class="form-control" name="prodprice" value="<?php echo set_value('prodprice', $products['prodprice']);?>"/>
		</div>
		<div class="img-file">
			<label for="prodimg">Immagine</label>
			<div class="mb-3">
				<img style="width: 100px;" src="<?php echo asset_url();?>img/products/<?php echo $products['prodimg']; ?>">
			</div>
			<input type="file" class="img-file-input" id="prodimg" name="prodimg">
		</div>
    </fieldset>
    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save</button>
<?php echo form_close(); ?>
