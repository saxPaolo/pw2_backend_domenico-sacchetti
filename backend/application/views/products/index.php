<div class="d-flex justify-content-between py-5">
    <h2>
        <?php echo $title; ?>
    </h2>
    <p>
        <a class="btn btn-success" href="<?php echo site_url('products/create/'); ?>"><i class="fas fa-plus"></i> Nuovo</a>
    </p>
</div>

<?php 
    $feedback = $this->session->flashdata('feedback');

    if($feedback) :
?>
<div class="alert alert-success" role="alert">
    <p><?php echo $feedback ?></p>
</div>
<?php endif; ?>

<div class="table-responsive">
	<table class="table products_list">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Descrizione</th>
				<th>Immagine</th>
				<th>Prezzo</th>
				<th>Azioni</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($products as $product): ?>
			<tr id="item-<?php echo $product['id']; ?>">
				<td class="product_name"><strong><?php echo $product['prodname']; ?></strong></td>
				<td class="product_desc"><?php echo $product['proddesc']; ?></td>
				<td class="product_img"><img style="width: 100px;" src="<?php echo asset_url();?>img/products/<?php echo $product['prodimg']; ?>"></td>
				<td class="product_price"><?php echo $product['prodprice']; ?> €</td>
				<td class="actions">
					<ul class="list-inline mb-0">
						<li class="list-inline-item">
							<a class="btn btn-info" title="Modifica prodotto" href="<?php echo site_url('products/edit/'.$product['id']); ?>"><i class="fas fa-edit"></i></a>
						</li>
						<li class="list-inline-item">
							<a class="btn btn-warning" title="Elimina prodotto" href="<?php echo site_url('products/delete/'.$product['id']); ?>"><i class="fas fa-trash-alt"></i></a>
						</li>
					</ul>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
