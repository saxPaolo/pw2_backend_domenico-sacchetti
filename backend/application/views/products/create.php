<div class="d-flex justify-content-between py-5">
    <h2>
        <?php echo $title; ?>
    </h2>
</div>

<?php $validation_error = validation_errors(); ?>
<?php if($validation_error) : ?>
<div class="alert alert-warning" role="alert">
	<?php echo $validation_error; ?>
</div>
<?php endif; ?>

<?php echo form_open('products/create'); ?>
    <div class="form-group">
        <label for="prodname">Nome</label>
        <input id="prodname" type="text" class="form-control" name="prodname" value="<?php echo set_value('prodname');?>"/>
    </div>
    <div class="form-group">
        <label for="proddesc">Descrizione</label>
        <textarea id="proddesc" class="form-control" name="proddesc"><?php echo set_value('proddesc');?></textarea>
    </div>
    <div class="form-group">
        <label for="prodprice">Prezzo</label>
        <input id="prodprice" type="text" class="form-control" name="prodprice" value="<?php echo set_value('prodprice');?>"/>
	</div>
	<div class="img-file mb-3">
		<label for="prodimg">Immagine</label><br>		
		<input type="file" class="img-file-input" id="prodimg" name="prodimg" value="<?php echo set_value('prodimg');?>">
	</div>
    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save</button>
<?php echo form_close(); ?>
