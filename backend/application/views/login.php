<h2><?php echo $title; ?></h2>

<?php $validation_error = validation_errors(); ?>
<?php if($validation_error) : ?>
<div class="alert alert-warning" role="alert">
<?php echo $validation_error; ?>
</div>
<?php endif; ?>

<?php echo form_open('auth/login'); ?>
    <div class="form-group">
        <label for="input_username">Username</label>
        <input id="input_username" type="text" class="form-control" name="username" value="<?php echo set_value('username','');?>"/>
    </div>
    <div class="form-group">
        <label for="input_password">Password</label>
        <input id="input_password" type="password" class="form-control" name="password"/>
    </div>
    <button type="submit" class="btn btn-success"><i class="fas fa-lock"></i> Login</button>
<?php echo form_close(); ?>
