<div class="d-flex justify-content-between py-5">
    <h2>
        Id ordine: #
        <?php echo $id; ?>
	</h2>
	<p>
        <a href="<?php echo site_url('orders/index/'); ?>"><i class="fas fa-list-ul"></i> Torna agli ordini</a>
    </p>
</div>

<div class="row order-single">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <header>
			<h4>Dati dell'ordine</h4>
		</header>
        <dl>
            <dt>Data:</dt>
            <dd>
                <?php echo $data_ordine; ?>
            </dd>
            <dt>Indirizzo</dt>
            <dd>
                <?php echo $indirizzo . ", " . $numero . " - " . $citta; ?>
            </dd>
            <dt>Email:</dt>
            <dd>
                <?php echo $email; ?>
            </dd>
            <dt>Telefono:</dt>
            <dd>
                <?php echo $tel; ?>
            </dd>
            <dt>Stato:</dt>
            <dd>
				<?php echo $status; ?> <i class="fas fa-<?php echo $status_icon; ?> <?php echo $status_class; ?>"></i>
            </dd>
        </dl>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		<header>
			<h4>Prodotti ordinati</h4>
		</header>
        <div class="table-responsive order-single-table">
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="2">Prodotto</th>
                        <th>Q.tà</th>
                        <th>Prezzo</th>
                        <th>Totale cad.</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $somma = 0; ?>
                    <?php foreach ($rows as $row) : ?>
                    <tr>
                        <td><?php echo $row['prodname']; ?></td>
                        <td><img style="width: 100px;" src="<?php echo asset_url();?>img/products/<?php echo $row['prodimg']; ?>"></td>
                        <td><?php echo $row['qtyprod']; ?></td>
                        <td><?php echo $row['prodprice']; ?> €</td>
                        <td>
                            <?php $totale_cad = $row['qtyprod']*$row['prodprice'] ?>
                            <?php echo number_format($totale_cad, 2, '.', '.'); ?> €
                        </td>
                    </tr>
                    <?php $somma += $totale_cad; ?>
                    <?php endforeach; ?>
                    <tr class="spese">
                        <td colspan="4">
                            Spese di consegna:
                        </td>
                        <td>
                            2.00 €
                        </td>
                    </tr>
                    <tr class="totale">
                        <td colspan="4">
                            <strong>Totale:</strong>
                        </td>
                        <td>
                            <?php $totale = $somma + 2; ?>
                            <?php echo number_format($totale, 2, '.', '.'); ?> €
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
