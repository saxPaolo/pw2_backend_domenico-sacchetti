<div class="d-flex justify-content-between py-5">
    <h2>
        <?php echo $title; ?>
    </h2>
</div>

<?php 
    $feedback = $this->session->flashdata('feedback');

    if($feedback) :
?>
<div class="alert alert-success" role="alert">
    <p><?php echo $feedback ?></p>
</div>
<?php endif; ?>
<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Data</th>
				<th>Orario</th>
				<th>Indirizzo</th>
				<th>Email</th>
				<th>Tel.</th>
				<th>Stato</th>
				<th>Azioni</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($orders as $order): ?>
			<tr>
				<td><?php echo $order['id']; ?></td>
				<td><?php echo data_it($order['orddata']); ?></td>
				<td><?php echo substr($order['ordorario'], 11); ?></td>
				<td><?php echo $order['ordindirizzo'] . ', ' . $order['ordstrnum'] . ' - ' . $order['ordcitta']; ?></td>
				<td><?php echo $order['ordemail']; ?></td>
				<td><?php echo $order['ordtel']; ?></td>
				<td><i class="fas fa-<?php echo $order['statusicon']; ?> <?php echo $order['statusclass']; ?>"></i> <?php echo $order['statusnome']; ?></td>
				<td>
					<ul class="list-inline mb-0">
						<li class="list-inline-item">
							<a class="btn btn-info" href="<?php echo site_url('orders/view/'.$order['id']); ?>"><i class="fas fa-eye"></i></a>
						</li>
						<li class="list-inline-item">
							<a class="btn btn-warning" href="<?php echo site_url('orders/delete/'.$order['id']); ?>"><i class="fas fa-trash-alt"></i></a>
						</li>
					</ul>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
	<div class="alert alert-danger">
		Nota: dopo aver dovuto modificare il db (nomi tabelle e tutti i nomi dei campi, nonché tutti i file dell'applicazione) a causa di RedBeans (utilizzato in Slim), la join applicata alle tabelle "ordini" e "ordprod" non funziona più a dovere: a quanto pare RedBeans richiede la presenza di un campo "id" in ciascuna tabella, mentre io avevo usato una nomenclatura più specifica, proprio come ci è stato insegnato durante il corso. Tra l'altro le tabelle non possono essere nominate in camelNotation (altrimenti il nome viene splittato e inserito il simbolo "-"). Sono convinto che esista un modo, e se avrò tempo cercherò di rimediare informandomi meglio. Ora non sono capace.
	</div>
	
</div>
