<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Piadineria ENAIP - la meglio piadina dopo quella di mia mamma</title>

    <!-- CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/4/flatly/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo asset_url();?>css/customs.css">

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed:700" rel="stylesheet">

	<!-- ICONS -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
</head>
<body id="<?php echo $section; ?>">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="<?php echo base_url(); ?>">Piadineria ENAIP</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item home">
						<a class="nav-link" href="<?php echo base_url(); ?>">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item product-list">
						<a class="nav-link" href="<?php echo base_url(); ?>products">Prodotti</a>
					</li>
					<li class="nav-item orders">
						<a class="nav-link" href="<?php echo base_url(); ?>orders">Ordini</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

		<!-- CONTAINER -->
		<div class="container">
