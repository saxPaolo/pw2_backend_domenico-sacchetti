CREATE DATABASE  IF NOT EXISTS `pw2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pw2`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pw2
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ordini`
--

DROP TABLE IF EXISTS `ordini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordini` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orddata` date NOT NULL,
  `ordindirizzo` varchar(255) NOT NULL,
  `ordstrnum` int(11) NOT NULL,
  `ordcitta` varchar(255) NOT NULL,
  `ordemail` varchar(255) NOT NULL,
  `ordtel` varchar(16) NOT NULL,
  `ordorario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ordimporto` decimal(19,2) NOT NULL,
  `ordstatus` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invoice_id_UNIQUE` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordini`
--

LOCK TABLES `ordini` WRITE;
/*!40000 ALTER TABLE `ordini` DISABLE KEYS */;
INSERT INTO `ordini` VALUES (2,'2018-05-14','via Criptonite',59,'Smallville','superman@dccomics.com','0541 456951','2018-05-21 22:54:16',0.00,2),(3,'2018-05-22','via Mia',16,'Miacitta','pippo@pippi.it','0541 951753','2018-05-22 09:00:12',0.00,1),(4,'2018-03-13','via Valturio',12,'Rimini','pippo@pippi.it','0541 368429','2018-03-13 12:00:00',10.00,3),(6,'2018-05-19','via Mannaggia',1,'Rimini','gianni@pinotto.it','0541 635241','2018-05-19 01:30:30',12.00,1);
/*!40000 ALTER TABLE `ordini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordprod`
--

DROP TABLE IF EXISTS `ordprod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordprod` (
  `id` int(11) NOT NULL,
  `idord` int(11) NOT NULL,
  `idprod` int(11) NOT NULL,
  `qtyprod` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_ord_prod_UNIQUE` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordprod`
--

LOCK TABLES `ordprod` WRITE;
/*!40000 ALTER TABLE `ordprod` DISABLE KEYS */;
INSERT INTO `ordprod` VALUES (1,1,4,1),(2,1,2,3),(3,1,11,1),(4,1,1,7),(5,2,1,2),(6,2,3,6),(7,2,16,1),(8,3,1,1),(9,3,7,3),(10,3,9,1),(11,3,11,1),(12,4,1,3),(13,4,6,2),(14,4,13,1),(15,4,19,6),(16,4,2,8),(17,4,9,3);
/*!40000 ALTER TABLE `ordprod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodotti`
--

DROP TABLE IF EXISTS `prodotti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodotti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prodname` varchar(255) NOT NULL,
  `proddesc` longtext NOT NULL,
  `prodprice` decimal(19,2) NOT NULL,
  `prodimg` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `prod_id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodotti`
--

LOCK TABLES `prodotti` WRITE;
/*!40000 ALTER TABLE `prodotti` DISABLE KEYS */;
INSERT INTO `prodotti` VALUES (1,'La Belpaese','Bresaola, Rucola, Grana',8.60,'belpaese.jpg'),(2,'La Semplice','Cotto, Mozzarella, Funghi',5.60,'semplice.jpg'),(3,'L\'Emilia','Crudo, Mozzarella, Pomodoro, Lattuga',7.00,'emilia.jpg'),(4,'La Freschezza','Salmone, Squacquerone, Lattuga',9.00,'freschezza.jpg'),(5,'La Leggerezza','Tacchino, Squacquerone, Pomodoro, Lattuga',5.30,'leggerezza.jpg'),(6,'L\'Estate','Bresaola, Ricotta, Zucchine grigliate, Crema all\'aceto balsamico',10.00,'estate.jpg'),(8,'La Sobrietà','Cotto, Edamer ',4.50,'sobrieta.jpg'),(9,'La Provenzale','Tonno, Mozzarella, Pomodoro, Origano, Maionese',3.50,'provenzale.jpg'),(10,'La Golosa','Crudo, Mozzarella, Gamberetti, Salsa cocktail, Lattuga',5.70,'golosa.jpg'),(11,'La Deliziosa','Speck, Gorgonzola, Noci, Erbette',8.10,'deliziosa.jpg'),(12,'La Saporita','Salame, Squacquerone, Rucola',4.90,'saporita.jpg'),(13,'L\'Eccellenza','Culatta, Mozzarella di bufala campana d.o.p., Pomodoro, Lattuga, Patè di olive',10.50,'eccellenza.jpg'),(14,'La Foresta','Coppa, Porcini, Grana, Erbette, Alpigiana',8.50,'foresta.jpg'),(15,'La Montagna','Salsiccia, Speck, Provola affumicata, Porcini, Lattuga, Salsa boscaiola',6.60,'montagna.jpg'),(17,'Il Contadino','Cotto, Brie, Lattuga',9.00,'contadino.jpg'),(18,'Il Piccante','Spianata calabra, Brie, Rucola',4.40,'piccante.jpg'),(19,'L\'Appetitoso','Bresaola, Ricotta, Pomodoro, Rucola',3.90,'appetitoso.jpg'),(20,'Il Soave','Tacchino, Provola affumicata, Asparagi, Paté d\'olive',7.30,'soave.jpg'),(21,'Il Capriccioso','Culatta, Brie, Carciofi, Pomodoro, Lattuga',4.90,'capriccioso.jpg'),(22,'L\'Orto','Squacquerone, Pomodoro, Melanzane condite, Zucchine grigliate',8.10,'orto.jpg'),(23,'La Campagna','Mozzarella, Brie, Lattuga, carciofi, Noci',5.50,'campagna.jpg'),(31,'nuova piadissima','lorem ipsum',13.50,'capriccioso.jpg'),(32,'piaderrima','pippo pippi',8.30,'golosa.jpg'),(33,'Altra piadina buonissima','Prova inserimento piadina',8.00,'foresta.jpg'),(34,'SuperPiadina','Lorem ipsum dolor sit amet, consectetur adipiscing elit.',7.00,'emilia.jpg'),(35,'Pizzissima buonissima 2','prova Pizzissima buonissima 2',11.00,'leggerezza.jpg'),(36,'Piadona','tutto con due uova sopratutto con due uova sopra',13.00,'golosa.jpg'),(37,'Altra Piadona','lorem ipsuim',14.00,'saporita.jpg'),(39,'Nuovissima piadissima 2','descrizione della Nuovissima piadissima 2',13.00,'estate.jpg'),(40,'Piadina Slim','Nuova piadina da Slim',13.00,'aia.jpg'),(41,'Il contadino 2','Testo da Slim',7.50,'contadino.jpg');
/*!40000 ALTER TABLE `prodotti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statusordine`
--

DROP TABLE IF EXISTS `statusordine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statusordine` (
  `id` int(11) NOT NULL,
  `statusnome` varchar(45) NOT NULL,
  `statusicon` varchar(45) NOT NULL,
  `statusclass` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_status_UNIQUE` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statusordine`
--

LOCK TABLES `statusordine` WRITE;
/*!40000 ALTER TABLE `statusordine` DISABLE KEYS */;
INSERT INTO `statusordine` VALUES (1,'Evaso','check-circle','text-success'),(2,'In consegna','shipping-fast','text-warning'),(3,'Reso','frown','text-secondary');
/*!40000 ALTER TABLE `statusordine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(45) NOT NULL,
  `user_isadmin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-29 10:21:25
